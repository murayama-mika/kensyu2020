<!DOCTYPE html>
<?php

  $DB_DSN = "mysql:host=localhost; dbname=mmurayama; charset=utf8";
  $DB_USER = "webaccess";
  $DB_PW = "toMeu4rH";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT *
                FROM test_table01";

  // echo $query_str;
  $sql = $pdo ->prepare($query_str);
  $sql ->execute();
  $result = $sql ->fetchAll();

?>

<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>DBテスト</title>
  </head>
  <body>

    <h1>DBテスト</h1>

    <pre>
      <!-- <?php var_dump($result); ?> -->
    </pre>


    <!-- テーブル -->
    <table border='1'>

      <tr>
        <th>ID</th>
        <th>メニュー名</th>
        <th>お値段</th>
        <th>一言</th>
      </tr>

    <?php
      foreach ($result as $each) {
        echo
        "<tr>" .
          "<td>" . $each['id'] . "</td>" .
          "<td>" . $each['dish_name'] . "</td>" .
          "<td>" . $each['price'] . "</td>" .
          "<td>" . $each['memo'] . "</td>" .
        "</tr>" ;

        // $each['dish_name'] . "：" . $each['price'] . "円";
        // echo "<hr/>";

      }
    ?>

    </table>

  </body>
</html>
