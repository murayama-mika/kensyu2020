<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>マルチループ2</title>
  </head>
  <body>
    <h1>マルチループ2</h1>

    <?php

      $servants = array(
        array(
          "rarity" => "★5",
          "class" => "ルーラー",
          "name" => "シャーロック・ホームズ",
          "card" => "QQAAB",
          "memo" => "スターで殴ルーラー",
        ),
        array(
          "rarity" => "★5",
          "class" => "キャスター",
          "name" => "マーリン",
          "card" => "QAAAB",
          "memo" => "グランドロクデナシ",
        ),
        array(
          "rarity" => "★3",
          "class" => "アーチャー",
          "name" => "エウリュアレ",
          "card" => "QQAAB",
          "memo" => "真の英雄（女神）",
        ),
        array(
          "rarity" => "★4",
          "class" => "ライダー",
          "name" => "マリー・アントワネット",
          "card" => "QQAAB",
          "memo" => "ヴィヴ・ラ・フランス",
        ),
        array(
          "rarity" => "★0",
          "class" => "アヴェンジャー",
          "name" => "アンリマユ",
          "card" => "QQAAB",
          "memo" => "トリッキーさしかない",
        ),
        array(
          "rarity" => "★5",
          "class" => "キャスター",
          "name" => "アルトリア・ペンドラゴン",
          "card" => "QAAAB",
          "memo" => "環境破壊人権キャスター",
        ),
        array(
          "rarity" => "★3",
          "class" => "キャスター",
          "name" => "クー・フーリン",
          "card" => "QAAAB",
          "memo" => "アーツ偏重型だけど宝具はバスター",
        ),
      );

        // foreach($servants as $each){
        //     echo "レアリティ：" . $each['rarity'] . ", "
        //         . "クラス：" . $each['class'] . ", "
        //         . "名前：" . $each['name'] . ", "
        //         . "カード構成：" . $each['card'] . ", "
        //         . "メモ：" . $each['memo'] . "<br/>";
        // }

    ?>

    <br>
    <hr>
    <br>

    <!-- テーブル -->
    <table border='1'>

      <tr>
        <th>レアリティ</th>
        <th>クラス</th>
        <th>名前</th>
        <th>カード構成</th>
        <th>メモ</th>
      </tr>

      <?php
        foreach($servants as $each){
          echo
            "<tr>" .
              "<td>" . $each['rarity'] . "</td>" .
              "<td>" . $each['class'] . "</td>" .
              "<td>" . $each['name'] . "</td>" .
              "<td>" . $each['card'] . "</td>" .
              "<td>" . $each['memo']  . "</td>" .
            "</tr>" ;
        }
      ?>

    </table>

  </body>
</html>
