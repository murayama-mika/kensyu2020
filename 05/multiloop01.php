<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>二次元配列</title>
  </head>
  <body>
    <h1>二次元配列</h1>

    <?php

      $team_yanagiya = array( "喬太郎" , "三三" , "花禄" , "小三治" , "小さん");
      $team_ryutei = array( "痴楽" , "市馬" , "小痴楽" , "こみち" , "一弥");
      $team_syunputei = array( "昇太" , "一之輔" , "一朝" , "昇也" , "小朝");
      $team_tatekawa = array( "談志" , "志らく" , "談四郎" , "晴の輔" , "こしら");
      $team_sannyutei = array( "圓生" , "円楽" , "歌る多" , "小遊三" , "好楽");

      $team_all = array($team_yanagiya, $team_ryutei , $team_syunputei , $team_tatekawa , $team_sannyutei);

      echo "<pre>";
      var_dump($team_all);
      echo "</pre>";

      echo "<table border='1'>";
        foreach ($team_all as $each) {
          echo "<tr>";
          foreach ($each as $value) {
            echo "<td>" . $value . "</td>";
          }
          echo "</tr>";
        }
      echo "</table>";

     ?>

  </body>
</html>
