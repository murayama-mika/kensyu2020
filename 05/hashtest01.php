<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>ハッシュテーブル</title>
  </head>
  <body>
    <h1>ハッシュテーブル</h1>

    <?php
      // サンプル

      // 書き方その１
        $me_data = array(
              'fruit' => 'レモン',
              'sweets' => 'ブッシュドノエル',
              'princess' => 'カグヤヒメ',
              'group' => '裸鰓類',
              'drink' => 'イチゴミルク'
              );

      // // 書き方その２（php5.4以降)
      //   $me_data = [
      //         'fruit' => 'レモン',
      //         'sweets' => 'ブッシュドノエル',
      //         'princess' => 'カグヤヒメ',
      //         'group' => '裸鰓類',
      //         'drink' => 'イチゴミルク'
      //         ];
      //
      // // 書き方その３
      //   $me_data['fruit'] = 'レモン';
      //   $me_data['sweets'] = 'ブッシュドノエル';
      //   $me_data['princess'] = 'カグヤヒメ';
      //   $me_data['group'] = '裸鰓類';
      //   $me_data['drink'] = 'イチゴミルク';

      foreach ($me_data as $each) {
        echo $each . "<br/>";
      }

      echo "<hr/>";

      foreach ($me_data as $key => $value) {
        echo $key . "：" . $value . "<br/>";
      }

      echo "<hr/>";

      echo $me_data['princess']; // カグヤヒメ と表示される
      echo $me_data['group']; // '裸鰓類' と表示される
      echo $me_data['school']; // エラーになる
      $me_data['group'] = 25; // 上書きされる
      var_dump($me_data); // 配列の中身がすべて表示される

    ?>

    <hr/>

    <!-- ブラウザで表示させるときは<pre>で囲むと見やすく表示される -->
    <pre>
    <?php var_dump($me_data); ?>
    </pre>

  </body>
</html>
