<!DOCTYPE html>

<?php

  include("./include/statics.php"); // 出身地や性別を呼び出す
  include("./include/functions.php"); // DB、エラー処理を呼び出す

  //パラメータチェック
  $param_namae = "";
  if(isset($_POST['namae']) && $_POST['namae'] !=""){
    $param_namae = $_POST['namae'];
  }else{
    commonError();
  }

  $param_pref = "";
  if(isset($_POST['from']) && $_POST['from'] !=""){
    $param_pref = $_POST['from'];
  }else{
    commonError();
  }

  $param_sex = "";
  if (isset($_POST['sex']) && $_POST['sex'] !="") {
    $param_sex = $_POST['sex'];
  }else{
    commonError();
  }

  $param_age = "";
  if (isset($_POST['age']) && $_POST['age'] !="" && is_numeric($_POST['age'])) {
    $param_age = $_POST['age'];
  }else{
    commonError();
  }

  $param_section = "";
  if (isset($_POST['section']) && $_POST['section'] !="") {
    $param_section = $_POST['section'];
  }else{
    commonError();
  }

  $param_grade = "";
  if (isset($_POST['grade']) && $_POST['grade'] !="") {
    $param_grade = $_POST['grade'];
  }else{
    commonError();
  }

  $param_ID = "";
  if(isset($_POST['member_ID']) && $_POST['member_ID'] !=""){
    $param_ID = $_POST['member_ID'];
  }else{
    commonError();
  }

  $pdo = initDB(); //DBへ接続するための関数

  $query_str = "UPDATE member AS m SET
                      m.name = '" . $param_namae . "',
                      m.pref = '" . $param_pref . "',
                      m.seibetu = '" . $param_sex . "',
                      m.age = '" . $param_age . "',
                      m.section_ID = '" . $param_section . "',
                      m.grade_ID = '" . $param_grade . "'
                WHERE m.member_ID = " . $param_ID ;

  try{
     $sql = $pdo->prepare($query_str);
     $sql->execute();
  }catch(PDOException $e){
     print $e->getMessage();
  }

  echo $query_str;

  $url = "detail01.php?id=" . $param_ID;
  header('Location: ' . $url);
  exit;

 ?>
