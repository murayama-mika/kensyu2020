<!DOCTYPE html>

<?php

  include("./include/functions.php"); // DBを呼び出す

  // パラメータチェック
  $param_namae = "";
  if (isset($_GET['namae']) && $_GET['namae'] !="") {
    $param_namae = $_GET['namae'];
  }

  $param_sex = "";
  if (isset($_GET['sex']) && $_GET['sex'] !="") {
    $param_sex = $_GET['sex'];
  }

  $param_section = "";
  if (isset($_GET['section']) && $_GET['section'] !="") {
    $param_section = $_GET['section'];
  }

  $param_grade = "";
  if (isset($_GET['grade']) && $_GET['grade'] !="") {
    $param_grade = $_GET['grade'];
  }

  $pdo = initDB(); //DBへ接続するための関数
  $result_section = section(); //部署データを持ってくる関数
  $result_grade = grade(); //役職データを持ってくる関数

  //DBからデータを引っ張ってくる
  $query_str = "SELECT
                  member.member_ID,
                  member.name,
                  member.seibetu,
                  section1_master.section_name,
                  grade_master.grade_name

                FROM member
                LEFT JOIN section1_master ON section1_master.ID = member.section_ID
                LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                WHERE 1=1 "; //これ(WHERE 1=1)は全件表示


  // 入力フォームに入力した値を引っ張ってくる
  if ($param_namae != "") {
    $query_str .= " AND member.name LIKE '%" . $param_namae . "%' ";
  }

  if ($param_sex != "") {
    $query_str .= " AND member.seibetu = " . $param_sex;
  }

  if ($param_section != "") {
    $query_str .= " AND member.section_ID = " . $param_section;
  }

  if ($param_grade != "") {
    $query_str .= " AND member.grade_ID = " . $param_grade;

  }

  // SQLがどの値を持ってきているかを表示
  // echo $query_str;

  //resultに持ってきたデータを格納
  $sql = $pdo ->prepare($query_str);
  $sql ->execute();
  $result = $sql ->fetchAll();
?>
<!-- 前処理ここまで -->

<html>
  <head>
    <meta charset="utf-8">
    <title>社員一覧-社員名簿システム</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="./common.css">

      <!-- CSSここから -->
      <style type="text/css">

        /* テーブル列幅 */
        #table-id {
          width: 15% ;
        }
        #table-namae {
          width: 45% ;
        }
        #table-section {
          width: 20% ;
        }
        #table-grade {
          width: 20% ;
        }

        /* 検索結果 */
        #search-result{
          margin-top: 20px;
          padding-left: 15%;
        }

        /* 入力フォーム */
        #input-form{
          /* width: 500px; */
          text-align: center;
          margin: 0 auto;
          padding-top: 20px;
        }

        /* ボタン */
        #button-top{
          margin: 0 auto;
          text-align: center;
        }
        #input-button{
          padding: 5px 10px;
        }

      </style>
      <!-- CSSここまで -->

    <!-- 入力フォームクリア -->
    <script type="text/javascript">
      function goClear(){
        document.search.namae.value = "";
        document.search.sex.value = "";
        document.search.section.value = "";
        document.search.grade.value = "";
      }
    </script>
  </head>

  <!-- ヘッダー呼び出し -->
  <?php include("./include/header.php"); ?>

  <!-- Bootstrap-->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  <body>

    <!-- 入力部分 ここから -->
    <form name="search" method='GET' action='./index.php'>
      <div id="input-form">
        <p>
          <label for="namae">名前：</label>
            <input type="text" name="namae" value="<?php echo $param_namae; ?>">
          <br/>
          <label for="sex">性別：</label>
            <select name="sex">
              <option value=""<?php if ($param_sex == "") { echo "selected" ; } ?>>すべて</option>
              <option value="0"<?php if ($param_sex == "0") { echo "selected" ; } ?>>女</option>
              <option value="1"<?php if ($param_sex == "1") { echo "selected" ; } ?>>男</option>
            </select>
          <label for="section">部署：</label>
            <select name="section">
              <option value="">すべて</option>
              <?php
                $result_section = section();
                foreach ($result_section as $each) {
                  if ($each['ID'] == $param_section) {
                    echo "<option value='" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
                  }else{
                    echo "<option value='" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
                  }
                }
              ?>
            </select>
          <label for="grade">役職：</label>
            <select name="grade">
              <option value=""<?php if ($param_grade == "") { echo "selected" ; } ?>>すべて</option>
              <?php
                $result_grade = grade();
                foreach ($result_grade as $each) {
                  if ($each['ID'] == $param_grade) {
                    echo "<option value='" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
                  }else{
                    echo "<option value='" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
                  }
                }
              ?>
            </select>
        </p>
      </div>
      <p>
        <div id="button-top">
          <input class="input-button" id="input-button" type="submit" value="検索">
          <input class="input-button" id="input-button" type="button" value="リセット" onclick="goClear();">
        </div>
      </p>
    </form>
    <!-- 入力部分 ここまで -->

    <hr/>

    <!-- 出力部分 ここから -->
    <?php

      $pdo = initDB(); //DBへ接続するための関数

      $query_str = "SELECT
                      m.member_ID,
                      m.name,
                      m.pref,
                      m.seibetu,
                      sm.section_name,
                      gm.grade_name

                    FROM member AS m
                    LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                    LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                    WHERE 1=1
                     ";

      //PHPパラメーターチェック
      if($param_namae != ""){
        $query_str .= "AND m.name LIKE '%" . $param_namae . "%'";
      }

      if($param_sex != ""){
        $query_str .= "AND m.seibetu = " . $param_sex;
      }
      if($param_section != ""){
        $query_str .= " AND m.section_ID = " . $param_section;
      }
      if($param_grade != ""){
        $query_str .= " AND m.grade_ID = " . $param_grade;
      }

       $sql = $pdo->prepare($query_str);
       $sql->execute();
       $result = $sql->fetchAll();
    ?>

    <!-- 検索結果件数表示 -->
    <div id=search-result>
      検索結果：
      <?php
        $result_number = count($result);
        echo $result_number;
      ?>
    </div>

    <!-- テーブル -->
    <table class="table-sm">
      <tr>
        <th id="table-id">社員ID</th>
        <th id="table-namae">名前</th>
        <th id="table-section">部署</th>
        <th id="table-grade">役職</th>
      </tr>

      <?php
        if (count($result) == 0) {
          echo "<tr><td colspan='4'>" . "検索結果なし" . "</td></tr>";
        }
        else {
          foreach ($result as $each) {
            echo
              "<tr>"
                . "<td>" . $each['member_ID'] . "</td>"
                . "<td><a href='./detail01.php?id=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>"
                . "<td>" . $each['section_name'] . "</td>"
                . "<td>" . $each['grade_name'] . "</td>"
              . "</tr>" ;
          }
        }
      ?>
    </table>

  </body>

</html>
