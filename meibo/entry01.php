<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <title>社員新規登録-社員名簿システム</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="./common.css">

    <!-- CSS -->
    <style type="text/css">
      #table-entry01{
        margin-top: 100px;
      }
    </style>

    <!-- JavaScriptの呼び出し -->
    <script src="./include/functions.js"></script>

  </head>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  <?php
    include("./include/header.php"); // ヘッダー
    include("./include/statics.php"); // 出身地や性別を呼び出す
    include("./include/functions.php"); // DB、部署データ、役職データを呼び出す
    $pdo = initDB(); //DBへ接続するための関数
    $result_section = section(); //部署データを持ってくる関数
    $result_grade = grade(); //役職データを持ってくる関数

  ?>

  <!-- 前処理ここまで -->

  <body>

    <!-- 入力部分 ここから -->
    <form name="InputForm" method='POST' action='./entry02.php'>

      <!-- テーブル -->
      <table id='table-entry01'>

        <tr>
          <th><label for="namae">名前</label></th>
          <td><input type="text" name="namae" maxlength="30" placeholder="" value=""></td>
        </tr>

        <tr>
          <th><label for="from">出身地</label></th>
          <td>
            <select name="from">
              <option value="">都道府県</option>
              <?php
                foreach ($pref_array as $key => $value ) {
                  echo "<option name='from' value='" . $key . "'>" . $value . "</option>";
                }
              ?>
            </select>
          </td>
        </tr>

        <tr>
          <th><label for="sex">性別</label></th>
          <td>
            <label><input type="radio" name="sex" value="1" checked>男性&nbsp;</label>
            <label><input type="radio" name="sex" value="0">女性</label>
          </td>
        </tr>

        <tr>
          <th><label for="age">年齢</label></th>
          <td><input type="number" name="age" max="99" min="1"> 才</td>
        </tr>

        <tr>
          <th><label for="section">所属部署</label></th>
          <td>
            <?php
              $result_section = section();
              foreach ($result_section as $each) {
                if ($each['ID'] == 1) {
                  echo "<label><input type='radio' name='section' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'>" . $each['section_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>

        <tr>
          <th><label for="grade">役職</label></th>
          <td>
            <?php
              $result_grade = grade();
              foreach ($result_grade as $each) {
                if ($each['ID'] == 1) {
                  echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>

      </table>

      <div class="button">
        <input name='id' type='hidden' value='<?php echo $result[0]['member_ID']; ?>'>
        <input class="input-button" type="button" value="登録" onclick="goCheck();">
        <input class="input-button" type="reset" value="リセット">
      </div>

    </form>
    <!-- 入力部分 ここまで -->

  </body>

</html>
