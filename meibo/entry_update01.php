<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <title>社員情報編集-社員名簿システム</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="./common.css">
    <script src="./include/functions.js"></script>

    <!-- CSS -->
    <style type="text/css">
      #table-entry_update01{
        margin-top: 100px;
      }
    </style>

  </head>

  <!-- ヘッダーを呼び出す -->
  <?php include("./include/header.php"); ?>

  <?php

    include("./include/statics.php"); // 出身地や性別を呼び出す
    include("./include/functions.php"); // DB、エラー処理を呼び出す

    // パラメータチェック、エラー処理
    $param_ID = "";
    if (isset($_GET['id']) && $_GET['id'] !="" && is_numeric($_GET['id'])) {
      $param_ID = $_GET['id'];
    }
    else {
      commonError();
    }

    $pdo = initDB(); //DBへ接続するための関数
    $result_section = section(); //部署データを持ってくる関数
    $result_grade = grade(); //役職データを持ってくる関数

    $query_str = "SELECT
                    member.member_ID,
                    member.name,
                    member.pref,
                    member.seibetu,
                    member.age,
                    member.section_ID,
                    member.grade_ID
                  FROM member
                  WHERE member.member_ID = " . $param_ID
                  ;

    // どの値をもってきているかを表示
    // echo $query_str;

    $sql = $pdo ->prepare($query_str);
    $sql ->execute();
    $result = $sql ->fetchAll();

  ?>


  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  <!-- ここまで前処理 -->

  <body>
    <form name="InputForm" action='./entry_update02.php' method='POST'>
      <table id='table-entry_update01'>

        <tr>
          <th><label for="member_id">社員ID</label></th>
            <td>
              <?php echo $result[0]['member_ID']; ?>
            </td>
        </tr>

        <tr>
          <th><label for="namae">名前</label></th>
            <td>
              <input type="text" name="namae" maxlength="30" value="<?php echo $result[0]['name']; ?>">
            </td>
        </tr>

        <tr>
          <th><label for="from">出身地</label></th>
            <td>
              <select name="from">
                <?php
                  foreach ($pref_array as $key => $value ) {
                    if ($result[0]['pref'] == $key) {
                      echo "<option name='from' value='" . $key . "' selected>" . $value . "</option>" ;
                    } else {
                    echo "<option name='from' value='" . $key . "'>" . $value . "</option>" ;
                    }
                  }
                ?>
              </select>
            </td>
        </tr>

        <tr>
          <th><label for="sex">性別</label></th>
            <td>
              <label><input name='sex' type="radio" value="1" <?php if ($result[0]['seibetu'] == "1") { echo 'checked' ; } ?>>男&nbsp;</label>
              <label><input name='sex' type="radio" value="0" <?php if ($result[0]['seibetu'] == "0") { echo 'checked' ; } ?>>女</label>
            </td>
        </tr>

        <tr>
          <th><label for="age">年齢</label></th>
            <td>
              <input type="number" name="age" max="99" min="1" value="<?php echo $result[0]['age']; ?>"> 才
            </td>
        </tr>

        <tr>
          <th><label for="section">所属部署</label></th>
          <td>
            <?php
              $result_section = section();
              foreach ($result_section as $each) {
                if ($each['ID'] == $result[0]['section_ID']) {
                  echo "<label><input type='radio' name='section' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'>" . $each['section_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>

        <tr>
          <th><label for="grade">役職</label></th>
            <td>
              <?php
                $result_grade = grade();
                foreach ($result_grade as $each) {
                  if ($each['ID'] == $result[0]['grade_ID']) {
                    echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "</label>　";
                  }else{
                    echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</label>　";
                  }
                }
              ?>
            </td>
        </tr>

      </table>

      <!-- ボタン -->
      <div class="button" id="button-entry_update01">
        <input name='member_ID' type='hidden' value='<?php echo $result[0]['member_ID'] ?>'>
        <input class="input-button" type="button" value="編集" onclick="goCheck();">
        <input class="input-button" type="reset" value="リセット">
      </div>

    </form>

  </body>
</html>
