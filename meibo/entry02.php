<!DOCTYPE html>

<?php

  include("./include/statics.php"); // 出身地や性別を呼び出す
  include("./include/functions.php"); // DB、エラー処理を呼び出す

  //パラメータチェック
  $param_namae = "";
  if(isset($_POST['namae']) && $_POST['namae'] !=""){
    $param_namae = $_POST['namae'];
  }else{
    commonError();
  }

  $param_pref = "";
  if(isset($_POST['from']) && $_POST['from'] !=""){
    $param_pref = $_POST['from'];
  }else{
    commonError();
  }

  $param_sex = "";
  if (isset($_POST['sex']) && $_POST['sex'] !="") {
    $param_sex = $_POST['sex'];
  }else{
    commonError();
  }

  $param_age = "";
  if (isset($_POST['age']) && $_POST['age'] !="") {
    $param_age = $_POST['age'];
  }else{
    commonError();
  }

  $param_section = "";
  if (isset($_POST['section']) && $_POST['section'] !="") {
    $param_section = $_POST['section'];
  }else{
    commonError();
  }

  $param_grade = "";
  if (isset($_POST['grade']) && $_POST['grade'] !="") {
    $param_grade = $_POST['grade'];
  }else{
    commonError();
  }

  $pdo = initDB(); //DBへ接続するための関数

  // DBに新規登録
  $query_str = "INSERT INTO member
                  (member_ID, name, pref, seibetu, age, section_ID, grade_ID)
                VALUES
                  ( NULL ,
                    '" . $param_namae . "',
                    '" . $param_pref . "', '" .
                    $param_sex . "' , '" .
                    $param_age . "' , '" .
                    $param_section . "' , '" .
                    $param_grade . "' ) " ;

  //例外処理を拾う
  try{
     $sql = $pdo->prepare($query_str);
     $sql->execute();

     $param_ID = $pdo ->lastInsertID('member_ID'); //AIで生成したIDを取得

   }catch(PDOException $e){ //PDO：DBと通信するときに使うもの Exception：例外 $e:エラー用の変数
     print $e->getMessage();
   }

  // SQLの生データ表示
  // echo $query_str;

  //社員詳細画面への遷移。新規登録したIDを末尾に付与して遷移させる
  $url = "detail01.php?id=" . $param_ID;
  header('Location: ' . $url);
  exit;

?>
