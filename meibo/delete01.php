<!DOCTYPE html>

<?php

  include("./include/functions.php"); // DB、エラー処理を呼び出す

  // パラメータチェック
  $param_ID = "";
  if(isset($_POST['id']) && $_POST['id'] !=""){
    $param_ID = $_POST['id'];
  }else{
    commonError();
  }

  $pdo = initDB(); // DBへ接続するための関数

  // DBから削除
  $query_str = "DELETE FROM member
                WHERE member.member_ID = " . $param_ID ;

  // try catch：例外処理を拾う
  try{
     $sql = $pdo->prepare($query_str);
     $sql->execute();

   }catch(PDOException $e){ //PDO：DBと通信するときに使うもの Exception：例外 $e:エラー用の変数
     print $e->getMessage();
   }

  // SQLの生データ表示
  // echo $query_str;

  // 社員一覧画面への遷移（新規登録したIDを末尾に付与して遷移させる）
  $url = "./index.php";
  header('Location: ' . $url);
  exit;

?>
