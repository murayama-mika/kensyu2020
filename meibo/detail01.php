<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <title>社員詳細-社員名簿システム</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="./common.css">

    <!-- CSSここから -->
    <style type="text/css">

      /* テーブル位置 */
      #table-detail01 {
        margin-top: 100px;
      }

      /* ボタン位置 */
      #button-detail{
        margin: 0px 43%;
        display: inline-flex;
      }

    </style>
    <!-- CSSここまで -->

      <script type="text/javascript">
        function goDel(){
          var result = window.confirm('削除を行います。よろしいですか？');
            if(result){
              document.delete.submit();
            }
        }
      </script>

  </head>

  <!-- ヘッダー呼び出し -->
  <?php include("./include/header.php"); ?>

  <?php

    include("./include/statics.php"); // 出身地や性別を呼び出す
    include("./include/functions.php"); // DB、エラー処理を呼び出す

    // パラメータチェック、エラー処理
    $param_ID = "";
    if (isset($_GET['id']) && $_GET['id'] !="" && is_numeric($_GET['id'])) {
      $param_ID = $_GET['id'];
    }
    else {
      commonError();
    }

    $pdo = initDB(); //DBへ接続するための関数

    $query_str = "SELECT
                    member.member_ID,
                    member.name,
                    member.pref,
                    member.seibetu,
                    member.age,
                    section1_master.section_name,
                    grade_master.grade_name

                  FROM member
                  LEFT JOIN section1_master ON section1_master.ID = member.section_ID
                  LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                  WHERE member.member_ID = " . $param_ID

                  ;

    // どの値をもってきているかを表示
    // echo $query_str;

    //
    $sql = $pdo ->prepare($query_str);
    $sql ->execute();
    $result = $sql ->fetchAll();

    //1件以上取得したときのエラー処理
    if (count($result) != 1 ) {
      commonError();
    }

  ?>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  <!-- ここまで前処理 -->
  <body>

    <!-- テーブル -->
    <table id='table-detail01'>
      <tr>
        <th><label for="member_id">社員ID</label></th>
          <td>
            <?php echo $result[0]['member_ID']; ?>
          </td>
      </tr>
      <tr>
        <th><label for="namae">名前</label></th>
          <td>
            <?php echo $result[0]['name']; ?>
          </td>
          </td>
      </tr>
      <tr>
        <th><label for="from">出身地</label></th>
          <td>
            <?php echo $pref_array[$result[0]['pref']]; ?>
          </td>
      </tr>
      <tr>
        <th><label for="sex">性別</label></th>
          <td>
            <?php echo $gender_array[$result[0]['seibetu']]; ?>
          </td>
      </tr>
      <tr>
        <th><label for="age">年齢</label></th>
          <td>
            <?php echo $result[0]['age']; ?>才
          </td>
      </tr>
      <tr>
        <th><label for="section">所属部署</label></th>
          <td>
            <?php echo $result[0]['section_name']; ?>
          </td>
      </tr>
      <tr>
        <th><label for="grade">役職</label></th>
          <td>
            <?php echo $result[0]['grade_name']; ?>
          </td>
      </tr>
    </table>

    <!-- ボタン -->
    <div id="button-detail">
      <form action='./entry_update01.php' method='GET'>
        <input name='id' type='hidden' value='<?php echo $result[0]['member_ID'] ?>'>
        <input class="input-button" type="submit" value="編集">
      </form>
      <form name='delete' action='./delete01.php' method='POST'>
        <input name='id' type='hidden' value='<?php echo $result[0]['member_ID'] ?>'>
        <input class="input-button" type="button" value="削除" onclick="goDel();">
      </form>
    </div>

  </body>
</html>
