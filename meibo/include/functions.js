//入力チェック

function goCheck(){

  var temp_name = document.InputForm.namae.value;
  temp_name = temp_name.replace(/\s+/g,"");
  document.InputForm.namae.value = temp_name;
  if(temp_name == ""){
    window.alert('名前を入力してください');
    return false ;
  }

  if(document.InputForm.from.value == ""){
    window.alert('出身地を選択してください');
    return false ;
  }

  var regex = new RegExp(/^[0-9]+$/);
  if(document.InputForm.age.value == "" || document.InputForm.age.value < 1 || document.InputForm.age.value > 99 || !regex.test(document.InputForm.age.value)){
    window.alert('年齢を1～99の整数で入力してください');
    return false ;
  }

  if (window.confirm('この内容で登録します。よろしいですか？')){
    document.InputForm.submit();
  }

}
