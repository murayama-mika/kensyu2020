<?php

  //エラー処理
  function commonError(){
    echo "エラーが発生しました。<br/>";
    echo "<a href='./index.php'>トップページに戻る</a>";
    echo "</body>";
    echo "</html>";
    exit();
  }

  //DBへの接続処理
  function initDB(){
    $DB_DSN = "mysql:host=localhost; dbname=mmurayama; charset=utf8";
    $DB_USER = "webaccess";
    $DB_PW = "toMeu4rH";
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    return $pdo;
  }

  // 部署一覧
  function section(){
    $pdo = initDB();
    $query_str = "SELECT *
                  FROM section1_master
                  ";
     $sql = $pdo->prepare($query_str);
     $sql->execute();
     $result_section = $sql->fetchAll();

    return $result_section;
  }

  // 役職一覧
  function grade(){
    $pdo = initDB();
    $query_str = "SELECT *
                  FROM grade_master
                   ";
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result_grade = $sql->fetchAll();

    return $result_grade;
  }


?>
