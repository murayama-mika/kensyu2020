<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>配列</title>
  </head>
  <body>
    <h1>配列</h1>

    <!-- ちなみに表示されている食べ物の名前は全部ウミウシくんの名前です -->

    <?php
      // サンプル
      $food = array("イチゴミルク", "コンペイトウ", "ブッシュドノエル", "キャラメル", "レモン", "グレープ" );

      echo $food[3]; // ブッシュドノエル と表示される

      echo $food[0]; // イチゴミルク と表示される

      echo $food[9]; // エラーになる

      $food[2] = "フルーツポンチ"; // 上書きされる

      $food[6] = "メレンゲ"; // 追加される

      var_dump($food); // 配列の中身がすべて表示される

      echo "<table border=\"1\">"; //属性の値はシングルクォート（''）、もしくは\"\"で括ってあげる
      for($i=0; $i < count($food) ; $i++){ //count($food) ： 配列の個数を返す（現在7個入っているから、7になるまで繰り返す）
        echo "<tr><td>" . $food[$i] . "</td></tr>";
      }
      echo "</table>";

      echo "<hr/>";

      foreach ($food as $key => $value) {
        echo $key . "番目の要素は" . $value ."ウミウシです。<br/>";
      }

      echo "<hr/>";

      // 検索する文字列
      $needle = "メレンゲ";

      if (in_array($needle, $food)) {
        echo $needle . " がfoodの要素の値に存在しています。" ;
      } else {
        echo $needle . " がfoodの要素の値に存在していません。" ;
      }

        ?>

      <br>

      配列の個数は<?php echo count($food); ?>です！


    <!-- ブラウザで表示させるときは<pre>で囲むと見やすく表示される -->
    <!-- var_dump：中身を見やすく整える -->
    <pre>
    <?php var_dump($food); ?>
    </pre>

  </body>
</html>
