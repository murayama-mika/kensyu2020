<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>ループ</title>
  </head>
  <body>
    <h1>ループ処理</h1>

    <form class="" action="./loop01.php" method="GET">

      <!-- 入力部分 --->
      <p><input type="number" name="number" placeholder="半角数字">行のテーブルを生成する</p>
      <p>
        <input type="submit" value="送信">
        <input type="reset" value="リセット">
      </p>

      <!-- 出力部分 --->
      <table border="1">
        <?php
          for($i=0; $i < $_GET['number']; $i++){

            if($i % 2 == 0){
              echo "<tr style='background-color: #9fbed9;'> <td>ウミウシ</td> <td>シースラッグ</td> <td>海の宝石</td> </tr>";

            } else{
              echo "<tr style='background-color: #e1f7f3;'> <td>ウミウシ</td> <td>シースラッグ</td> <td>海の宝石</td> </tr>";
            }

          }
        ?>
      </table>

    </form>
  </body>
</html>
