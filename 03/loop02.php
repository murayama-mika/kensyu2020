<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport' content="width=device-width, initial-scale=1">
		<title>ループ 2</title>
	</head>
	<body>
		<h1>ループ処理 2</h1>

		<form class="" action="./loop02.php" method="GET">

			<!-- 入力部分 --->
			<input type="number" name="number_01" placeholder="半角数字">行
				×
			<input type="number" name="number_02" placeholder="半角数字">列
			<p>
				<input type="submit" value="送信">
				<input type="reset" value="リセット">
			</p>

		</form>

			<!-- 出力部分 --->
			<table border="1">
				<?php
					for ($i=0; $i < $_GET['number_01']; $i++){
						echo "<tr>";
						for ($j=0; $j < $_GET['number_02']; $j++){
							echo "<td>" . $i . "-" . $j . "</td>";
						}
						echo "</tr>";
					}
				?>
			</table>

	</body>
</html>
