<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>ログイン 2</title>
  </head>
  <body>
    <h1>ログイン 2</h1>
    <form class="" action="result02.php" method="POST">
      <table border="0">
        <tr><th>ID</th></tr>
        <tr><td><input type="text" name="id" placeholder="ID（半角英数字）"></td></tr>
        <tr><th>パスワード</th></tr>
        <tr><td><input type="password" name="password" placeholder="パスワード（半角英数字）"></td></tr>
      </table>
      <p>
        <input type="submit" value="ログイン">
        <input type="reset" value="リセット">
      </p>
    </form>
  </body>
</html>
