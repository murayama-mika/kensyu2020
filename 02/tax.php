<!DOCTYPE html> <!-- これはHTMLやで～宣言 --->
<html> <!-- こっからHTML書くで～宣言 --->
  <head> <!-- ここからヘッド部分やで～ --->
    <meta charset='utf-8'> <!-- 文字コードこれやで～ --->
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>消費税計算</title> <!-- タイトル（タブの名前に表示されるやつ）やで～ --->
  </head>

  <body><!-- bodyここから --->
    <h1>消費税計算</h1>

      <!-- ここから入力部分の記述 --->

      <div class="input"><!-- 入力部分のクラス宣言 --->
        <form method='POST' action='tax.php'> <!-- form指定 --->
          <table border="1" style="border-collapse: collapse"><!-- 表スタイル指定 --->

            <div class="tabletop"><!-- 入力部分の表 上部 --->
              <tr>
                <th><label for="namae">商品名</label></th>
                <th><label for="price">価格（単位：円、税抜き）</label></th>
                <th><label for="number">個数</label></th>
                <th><label for="tax">税率</label></th>
              </tr>
            </div>

            <div class="tablebottom"><!-- 入力部分の表 下部 --->
              <tr>
                <td><input type="text" name="namae1"></td>
                <td><input type="text" name="price1"></td>
                <td><input type="text" name="number1">個</td>
                <td>
                  <label><input type="radio" name="tax1" value="8">8%</label>
                  <label><input type="radio" name="tax1" value="10">10%</label>
                </td>
              </tr>
              <tr>
                <td><input type="text" name="namae2"></td>
                <td><input type="text" name="price2"></td>
                <td><input type="text" name="number2">個</td>
                <td><label><input type="radio" name="tax2" value="8">8%</label>
                    <label><input type="radio" name="tax2" value="10">10%</label></td>
              </tr>
              <tr>
                <td><input type="text" name="namae3"></td>
                <td><input type="text" name="price3"></td>
                <td><input type="text" name="number3">個</td>
                <td><label><input type="radio" name="tax3" value="8">8%</label>
                    <label><input type="radio" name="tax3" value="10">10%</label></td>
              </tr>
              <tr>
                <td><input type="text" name="namae4"></td>
                <td><input type="text" name="price4"></td>
                <td><input type="text" name="number4">個</td>
                <td><label><input type="radio" name="tax4" value="8">8%</label>
                    <label><input type="radio" name="tax4" value="10">10%</label></td>
              </tr>
              <tr>
                <td><input type="text" name="namae5"></td>
                <td><input type="text" name="price5" ></td>
                <td><input type="text" name="number5" >個</td>
                <td><label><input type="radio" name="tax5" value="8">8%</label>
                    <label><input type="radio" name="tax5" value="10">10%</label></td>
              </tr>
            </div>
          </table>

            <input type="submit" value="送信">
            <input type="reset" value="リセット">

        </form>

        <br>

      </div>

      <!-- ここから出力部分の記述 --->

      <table border="1" style="border-collapse: collapse"><!-- 表スタイル指定 --->

        <!-- 入力部分の表 上部 --->
        <tr>
          <th><label for="namae">商品名</label></th>
          <th><label for="price">価格（単位：円、税抜き）</label></th>
          <th><label for="number">個数</label></th>
          <th><label for="tax">税率</label></th>
          <th><label for="subtotal">小計（単位：円）</label></th>
        </tr>

        <!-- 入力部分の表 下部 --->
        <tr>
          <td><?php echo $_POST['namae1'];?></td>
          <td><?php echo $_POST['price1'];?></td>
          <td><?php echo $_POST['number1'];?></td>
          <td><?php echo $_POST['tax1'] . "%" ;?></td>
          <td><?php $taxtotal1 = $_POST['tax1'] / 100 + 1 ;?>
              <?php $subtotal1 = $_POST['price1'] * $_POST['number1'] * $taxtotal1 ;?>
              <?php echo $subtotal1 ;?></td>
        </tr>
        <tr>
          <td><?php echo $_POST['namae2'];?></td>
          <td><?php echo $_POST['price2'];?></td>
          <td><?php echo $_POST['number2'];?></td>
          <td><?php echo $_POST['tax2'] . "%" ;?></td>
          <td><?php $taxtotal2 = $_POST['tax2'] / 100 + 1 ;?>
              <?php $subtotal2 = $_POST['price2'] * $_POST['number2'] * $taxtotal2 ;?>
              <?php echo $subtotal2 ;?></td>
        </tr>
        <tr>
          <td><?php echo $_POST['namae3'];?></td>
          <td><?php echo $_POST['price3'];?></td>
          <td><?php echo $_POST['number3'];?></td>
          <td><?php echo $_POST['tax3'] . "%" ;?></td>
          <td><?php $taxtotal3 = $_POST['tax3'] / 100 + 1 ;?>
              <?php $subtotal3 = $_POST['price3'] * $_POST['number3'] * $taxtotal3 ;?>
              <?php echo $subtotal3 ;?></td>
        </tr>
        <tr>
          <td><?php echo $_POST['namae4'];?></td>
          <td><?php echo $_POST['price4'];?></td>
          <td><?php echo $_POST['number4'];?></td>
          <td><?php echo $_POST['tax4'] . "%" ;?></td>
          <td><?php $taxtotal4 = $_POST['tax4'] / 100 + 1 ;?>
              <?php $subtotal4 = $_POST['price4'] * $_POST['number4'] * $taxtotal4 ;?>
              <?php echo $subtotal4 ;?></td>
        </tr>
        <tr>
          <td><?php echo $_POST['namae5'];?></td>
          <td><?php echo $_POST['price5'];?></td>
          <td><?php echo $_POST['number5'];?></td>
          <td><?php echo $_POST['tax5'] . "%" ;?></td>
          <td><?php $taxtotal5 = $_POST['tax5'] / 100 + 1 ;?>
              <?php $subtotal5 = $_POST['price5'] * $_POST['number5'] * $taxtotal5 ;?>
              <?php echo $subtotal5 ;?></td>
        </tr>

        <td colspan="4"><label for="total">合計</label></td><!-- 合計部 記述 --->
        <td><?php $total = $subtotal1 + $subtotal2 + $subtotal3 + $subtotal4 + $subtotal5 ;?>
            <?php echo $total ;?></td>
      </table>

  </body><!-- bodyここまで --->
</html>
