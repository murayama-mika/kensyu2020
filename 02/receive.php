<!DOCTYPE html> <!-- これはHTMLやで～宣言 --->
<html> <!-- こっからHTML書くで～宣言 --->
  <head> <!-- ここからヘッド部分やで～ --->
    <meta charset='utf-8'> <!-- 文字コードこれやで～ --->
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>入力内容確認</title> <!-- タイトル（タブの名前に表示されるやつ）やで～ --->
  </head>
  <body>

    <pre>
      <?php var_dump($_GET); ?>
    </pre>

    入力内容確認
    <p>
      名前：
      <?php
        echo $_GET['namae'];
      ?>
    </p>
    <p>
      メールアドレス：
      <?php
        echo $_GET['mail'];
      ?>
    </p>
    <p>
      パスワード：
      <?php
        echo $_GET['pass'];
      ?>
    </p>
    <p>
      お問い合わせ内容：
      <?php
        echo $_GET['select'];
      ?>
    </p>
    <p>
      ウミウシくんは好き？：
      <?php
        if(isset($_GET['radio'])) {
            $radio = $_GET['radio'];
            echo $radio . '<br>';
        } else {
            echo 'ラジオボタンを選択してください。<br>';
        }
      ?>
    </p>
    <p>
      好きなウミウシくんの種類は？（複数選択可）：
      <?php

      ?>
    </p>
    <p>
      タイトル：
      <?php
        echo $_GET['title'];
      ?>
    </p>
    <p>
      本文：
      <?php
        echo $_GET['text'];
      ?>
    </p>
    <p>
      記入内容のコピーを自分宛に送信する：
      <?php
        foreach ($_GET["check"] as $value) {
        echo "{$value}、";
        }
      ?>
    </p>

  </body>
</html>
