<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>＊＊＊これはただのメモ＊＊＊</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.4/css/all.css">
  </head>

  <pre>
    <!-- <?php var_dump($result); ?> -->
  </pre>


  <body>

  【SQL】
  マスターテーブルと結合して、任意のカラムを表示させるSQL。

  SELECT
   member.member_ID,
   member.name,
   member.pref,
   member.seibetu,
   section1_master.section_name,
   grade_master.grade_name
  FROM member
  LEFT JOIN section1_master ON section1_master.ID = member.section_ID
  LEFT JOIN grade_master ON grade_master.ID = member.grade_ID

  省略でもできる。

  SELECT
   m.member_ID,
   m.name,
   m.pref,
   m.seibetu,
   sm.section_name,
   gm.grade_name
   FROM member AS m
   LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
   LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID

  ↑

SELECT
  member.member_ID,
  member.name,
  section1_master.section_name,
  grade_master.grade_name
FROM member
LEFT JOIN section1_master ON section1_master.ID = member.section_ID
LEFT JOIN grade_master ON grade_master.ID = member.grade_ID



【SQLで絞り込む文（性別1、30歳以上で絞り込んだ場合）】

SELECT
  member.member_ID,
  member.name,
  member.age,
  section1_master.section_name,
  grade_master.grade_name

FROM member
LEFT JOIN section1_master ON section1_master.ID = member.section_ID
LEFT JOIN grade_master ON grade_master.ID = member.grade_ID

WHERE member.seibetu=1 AND member.age >=30


                // WHERE
                  // member.name LIKE '%" . $_GET['namae'] . "%' " . " AND
                  // member.seibetu = " . $_GET['sex'] . " AND
                  // member.section_ID = " . $_GET['section'] . " AND
                  // member.grade_ID = " . $_GET['grade'] ."
                // ";

if (isset($_GET['sex']) && $_GET['sex'] != "") {
  $query_str .= " AND member.seibetu = " . $_GET['sex'];
}



  </body>

</html>
